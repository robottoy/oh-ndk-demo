#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

void *add_task(void *arg) {
    int count = 0;

    while (1) {
        count++;
    }

    return arg;
}

int main(void) {
    pthread_t thread;

    if (pthread_create(&thread, /*attr*/NULL, add_task, /*arg*/NULL)) {
        perror("failed to create thread");
        return EXIT_FAILURE;
    }

    if (pthread_join(thread, /*retval*/NULL)) {
        perror("failed to join thread");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
