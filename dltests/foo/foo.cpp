#include <dlfcn.h>
#include <iostream>

extern "C" {

int add(int a, int b){
	return a+b;
}

}

class BarIns {
public:
	BarIns() {
		std::cerr << "BarIns()" << std::endl;
		handle = dlopen("./libbar.so", RTLD_NOW);
		if (!handle) {
			std::cerr << dlerror() << std::endl;
		}
	}

	~BarIns() {
		std::cerr << "~BarIns()" << std::endl;
		if (handle != 0) {
			dlclose(handle);
		}
	}

	static BarIns& GetInstance() {
		return BI;
	}

private:
	static BarIns BI;
	void *handle {0};
};

BarIns BarIns::BI;
