#include <iostream>
#include <dlfcn.h>

using ADD_FUNC=int(*)(int,int);

int test_recursive(int count) {
	void *handle = dlopen("./libfoo.so", RTLD_NOW);
	if (!handle) {
		std::cerr << dlerror() << std::endl;
		return -1;
	}

	ADD_FUNC add = (ADD_FUNC) dlsym(handle, "add");
	if (!add) {
		std::cerr << dlerror() << std::endl;
		return -1;
	}

	std::cout << add(1,1) << std::endl;

	dlclose(handle);

	std::cout << "count: " << count << std::endl;
	return 0;
}

static pthread_mutex_t lock = {{{1,0,0,0,0,0}}};

void* lock_and_run(void *arg) {
    pthread_mutex_lock(&lock);
    std::cout << "hello in thread" << std::endl;
    pthread_mutex_unlock(&lock);
    return NULL;
}

#define LOG_E(lock) { \
        printf("%p", lock); \
        pthread_mutex_lock(lock);\
        }

int main() {
    test_recursive(1);

    pthread_mutex_unlock(&lock);
    printf("%p\n", &lock);

    //pthread_mutex_lock(&lock);
    LOG_E(&lock);
    std::cout << "in lock" << std::endl;
    pthread_mutex_unlock(&lock);

    pthread_t newPid;
    pthread_create(&newPid, NULL, lock_and_run, NULL);

    pthread_join(newPid, NULL);
}
