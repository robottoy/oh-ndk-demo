#include "thumb_lib.h"

#include <stdio.h>

int main(void) {
    int x = 10;
    int y = 5;
    int product = mul_int(x, y);
    printf("x = %d, y = %d, x * y = %d\n", x, y, product);
    printf("x = %d, abs(x) = %d\n", x, abs_wrapper(x));
    return 0;
}
