#include "arm_lib.h"
#include "thumb_lib.h"

#include <stdlib.h>

int mul_int(int x, int y) {
    int res = 0;
    int num_iter = y >= 0 ? y : -y;
    int use_add = y >= 0;

    while (num_iter-- > 0) {
        if (use_add) {
            res = add_int(res, x);
        } else {
            res = sub_int(res, x);
        }
    }

    return res;
}

int abs_wrapper(int num) {
    return abs(num);
}
