#!/usr/bin/env sh

if [ -z $ANDROID_NDK_HOME ]; then
    echo "Error: cannot find Android NDK, environment variable ANDROID_NDK_HOME is not set or empty."
    echo "The value of ANDROID_NDK_HOME should be the directory of NDK. Make sure you have the NDK and set ANDROID_NDK_HOME properly."
    echo "For example, ANDROID_NDK_HOME can be set to '\$HOME/Android/Sdk/ndk/25.2.9519653'."
    exit 1
fi

curr_dir=$(cd $(dirname $0) && pwd)

aosp_ndk_cmake_toolchain_file=$ANDROID_NDK_HOME/build/cmake/android.toolchain.cmake
outdir=$curr_dir/out/aosp-arm64-build
srcdir=$curr_dir

cmake -B $outdir -S $srcdir -G Ninja \
    -DCMAKE_TOOLCHAIN_FILE=$aosp_ndk_cmake_toolchain_file \
    -DANDROID_ABI=arm64-v8a \
    $@

ninja -C $outdir
