#include <atomic>
#include <cstdlib>
#include <iostream>
#include <thread>

#define LOOP_ITERATION 1000

static int g_count {0};
static std::atomic<int> g_atomic_count {0};

static std::atomic_bool g_wait {true};

static void add_task() {
    while (g_wait)
        ;
    for (int i = 0; i < LOOP_ITERATION; i++) {
        g_count++;
        g_atomic_count++;
    }
}

int main(void) {
    std::thread worker(add_task);

    g_wait = false;
    for (int i = 0; i < LOOP_ITERATION; i++) {
        g_count++;
        g_atomic_count++;
    }
    worker.join();

    std::cout << "g_count = " << g_count << std::endl;
    return EXIT_SUCCESS;
}
